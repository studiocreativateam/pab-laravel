<?php

namespace PAB\Laravel;

use Illuminate\Support\ServiceProvider;

abstract class BaseServiceProvider extends ServiceProvider
{
    public static string $abstract = 'pab';

    protected function hasTokenSet(): bool
    {
        $config = $this->getUserConfig();
        return !empty($config['token']) && !empty($config['token']);
    }

    protected function getUserConfig(): array
    {
        $config = $this->app['config'][static::$abstract];

        return empty($config) ? [] : $config;
    }

    protected function couldHavePerformanceTracingEnabled(): bool
    {
        $config = $this->getUserConfig();

        return !empty($config['traces_sample_rate']) || !empty($config['traces_sampler']);
    }
}
