<?php

namespace PAB\Laravel;

final class Version
{
    public const SDK_IDENTIFIER = 'pab.php.laravel';
    public const SDK_VERSION = '1.0.0';
}
