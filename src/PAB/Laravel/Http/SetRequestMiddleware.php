<?php

namespace PAB\Laravel\Http;

use Closure;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use PAB\State\HubInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

class SetRequestMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $container = Container::getInstance();

        if ($container->bound(HubInterface::class)) {
            try {
                $container->instance(
                    LaravelRequestFetcher::CONTAINER_PSR7_INSTANCE_KEY,
                    $container->make(ServerRequestInterface::class)
                );
            } catch (Throwable $e) {
                // Ignore problems getting the PSR-7 server request instance here
                // In the Laravel request fetcher we have other fallbacks for that
            }
        }

        return $next($request);
    }
}
