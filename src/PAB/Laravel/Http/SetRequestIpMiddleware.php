<?php

namespace PAB\Laravel\Http;

use Closure;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use PAB\State\HubInterface;
use PAB\State\Scope;

class SetRequestIpMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $container = Container::getInstance();

        if ($container->bound(HubInterface::class)) {
            /** @var \PAB\State\HubInterface $pab */
            $pab = $container->make(HubInterface::class);

            $client = $pab->getClient();

            if ($client !== null && $client->getOptions()->shouldSendDefaultPii()) {
                $pab->configureScope(static function (Scope $scope) use ($request): void {
                    $scope->setUser([
                        'ip_address' => $request->ip(),
                    ]);
                });
            }
        }

        return $next($request);
    }
}
