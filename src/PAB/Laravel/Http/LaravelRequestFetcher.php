<?php

namespace PAB\Laravel\Http;

use Illuminate\Container\Container;
use Psr\Http\Message\ServerRequestInterface;
use PAB\Integration\RequestFetcher;
use PAB\Integration\RequestFetcherInterface;

class LaravelRequestFetcher implements RequestFetcherInterface
{
    public const CONTAINER_PSR7_INSTANCE_KEY = 'pab-laravel.psr7.request';

    public function fetchRequest(): ?ServerRequestInterface
    {
        $container = Container::getInstance();

        if (!$container->bound('request')) {
            return null;
        }

        if ($container->bound(self::CONTAINER_PSR7_INSTANCE_KEY)) {
            return $container->make(self::CONTAINER_PSR7_INSTANCE_KEY);
        }

        return (new RequestFetcher)->fetchRequest();
    }
}
