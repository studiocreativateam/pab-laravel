<?php

namespace PAB\Laravel\Tracing;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use PAB\Laravel\Integration;
use PAB\PABSdk;
use PAB\State\HubInterface;
use PAB\Tracing\Span;
use PAB\Tracing\SpanContext;
use PAB\Tracing\Transaction;
use PAB\Tracing\TransactionContext;

class Middleware
{
    protected ?Transaction $transaction;

    protected ?Span $appSpan;

    private ?float $bootedTimestamp;

    public function handle($request, Closure $next)
    {
       if (app()->bound(HubInterface::class)) {
            $this->startTransaction($request, app(HubInterface::class));
        }

        return $next($request);
    }

    public function terminate($request, $response): void
    {
        if ($this->transaction !== null && app()->bound(HubInterface::class)) {
            if ($this->appSpan !== null) {
                $this->appSpan->finish();
            }

            PABSdk::getCurrentHub()->setSpan($this->transaction);

            if ($request instanceof Request) {
                $this->hydrateRequestData($request);
            }

            if ($response instanceof Response) {
                $this->hydrateResponseData($response);
            }

            $this->transaction->finish();
        }
    }

    public function setBootedTimestamp(?float $timestamp = null): void
    {
        $this->bootedTimestamp = $timestamp ?? microtime(true);
    }

    private function startTransaction(Request $request, HubInterface $pab): void
    {
        $requestStartTime = $request->server('REQUEST_TIME_FLOAT', microtime(true));
        $pabTraceHeader = $request->header('pab-trace');

        $context = $pabTraceHeader
            ? TransactionContext::fromPabTrace($pabTraceHeader)
            : new TransactionContext;

        $context->setOp('http.server');
        $context->setData([
            'url' => '/' . ltrim($request->path(), '/'),
            'method' => strtoupper($request->method()),
        ]);
        $context->setStartTimestamp($requestStartTime);

        $this->transaction = $pab->startTransaction($context);

        // Setting the Transaction on the Hub
        PABSdk::getCurrentHub()->setSpan($this->transaction);

        $bootstrapSpan = $this->addAppBootstrapSpan($request);

        $appContextStart = new SpanContext();
        $appContextStart->setOp('laravel.handle');
        $appContextStart->setStartTimestamp($bootstrapSpan ? $bootstrapSpan->getEndTimestamp() : microtime(true));

        $this->appSpan = $this->transaction->startChild($appContextStart);

        PABSdk::getCurrentHub()->setSpan($this->appSpan);
    }

    private function addAppBootstrapSpan(Request $request): ?Span
    {
        if ($this->bootedTimestamp === null) {
            return null;
        }

        $laravelStartTime = defined('LARAVEL_START') ? LARAVEL_START : $request->server('REQUEST_TIME_FLOAT');

        if ($laravelStartTime === null) {
            return null;
        }

        $spanContextStart = new SpanContext();
        $spanContextStart->setOp('laravel.bootstrap');
        $spanContextStart->setStartTimestamp($laravelStartTime);
        $spanContextStart->setEndTimestamp($this->bootedTimestamp);

        $span = $this->transaction->startChild($spanContextStart);

        // Consume the booted timestamp, because we don't want to report the bootstrap span more than once
        $this->bootedTimestamp = null;

        // Add more information about the bootstrap section if possible
        $this->addBootDetailTimeSpans($span);

        return $span;
    }

    private function addBootDetailTimeSpans(Span $bootstrap): void
    {
        // This constant should be defined right after the composer `autoload.php` require statement in `public/index.php`
        // define('PAB_AUTOLOAD', microtime(true));
        if (!defined('PAB_AUTOLOAD') || !PAB_AUTOLOAD) {
            return;
        }

        $autoload = new SpanContext();
        $autoload->setOp('laravel.autoload');
        $autoload->setStartTimestamp($bootstrap->getStartTimestamp());
        $autoload->setEndTimestamp(PAB_AUTOLOAD);

        $bootstrap->startChild($autoload);
    }

    private function hydrateRequestData(Request $request): void
    {
        $route = $request->route();

        if ($route instanceof Route) {
            $this->updateTransactionNameIfDefault(Integration::extractNameForRoute($route));

            $this->transaction->setData([
                'name' => $route->getName(),
                'action' => $route->getActionName(),
                'method' => $request->getMethod(),
            ]);
        }

        $this->updateTransactionNameIfDefault('/' . ltrim($request->path(), '/'));
    }

    private function hydrateResponseData(Response $response): void
    {
        $this->transaction->setHttpStatus($response->status());
    }

    private function updateTransactionNameIfDefault(?string $name): void
    {
        // Ignore empty names (and `null`) for caller convenience
        if (empty($name)) {
            return;
        }

        // If the transaction already has a name other than the default
        // ignore the new name, this will most occur if the user has set a
        // transaction name themself before the application reaches this point
        if ($this->transaction->getName() !== TransactionContext::DEFAULT_NAME) {
            return;
        }

        $this->transaction->setName($name);
    }
}
