<?php

namespace PAB\Laravel\Tracing;

use Illuminate\Support\Str;
use PAB\Frame;
use PAB\FrameBuilder;
use PAB\Options;
use PAB\Serializer\RepresentationSerializerInterface;

class BacktraceHelper
{
    private Options $options;

    private FrameBuilder $frameBuilder;

    public function __construct(Options $options, RepresentationSerializerInterface $representationSerializer)
    {
        $this->options = $options;
        $this->frameBuilder = new FrameBuilder($options, $representationSerializer);
    }

    public function findFirstInAppFrameForBacktrace(array $backtrace): ?Frame
    {
        $file = Frame::INTERNAL_FRAME_FILENAME;
        $line = 0;

        foreach ($backtrace as $backtraceFrame) {
            $frame = $this->frameBuilder->buildFromBacktraceFrame($file, $line, $backtraceFrame);

            if ($frame->isInApp()) {
                return $frame;
            }

            $file = $backtraceFrame['file'] ?? Frame::INTERNAL_FRAME_FILENAME;
            $line = $backtraceFrame['line'] ?? 0;
        }

        return null;
    }

    public function getOriginalViewPathForFrameOfCompiledViewPath(Frame $frame): ?string
    {
        // Check if we are dealing with a frame for a cached view path
        if (!Str::startsWith($frame->getFile(), '/storage/framework/views/')) {
            return null;
        }

        // If for some reason the file does not exists, skip resolving
        if (!file_exists($frame->getAbsoluteFilePath())) {
            return null;
        }

        $viewFileContents = file_get_contents($frame->getAbsoluteFilePath());

        preg_match('/PATH (?<originalPath>.*?) ENDPATH/', $viewFileContents, $matches);

        // No path comment found in the file, must be a very old Laravel version
        if (empty($matches['originalPath'])) {
            return null;
        }

        return $this->stripPrefixFromFilePath($matches['originalPath']);
    }

    private function stripPrefixFromFilePath(string $filePath): string
    {
        foreach ($this->options->getPrefixes() as $prefix) {
            if (Str::startsWith($filePath, $prefix)) {
                return mb_substr($filePath, mb_strlen($prefix));
            }
        }

        return $filePath;
    }
}
