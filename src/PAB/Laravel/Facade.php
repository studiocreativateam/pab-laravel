<?php

namespace PAB\Laravel;

use PAB\State\HubInterface;

/**
 * @see \PAB\State\HubInterface
 *
 * @method static \PAB\ClientInterface|null getClient()
 * @method static \PAB\EventId|null getLastEventId()
 * @method static \PAB\State\Scope pushScope()
 * @method static bool popScope()
 * @method static void withScope(callable $callback)
 * @method static void configureScope(callable $callback)
 * @method static void bindClient(\PAB\ClientInterface $client)
 * @method static \PAB\EventId|null captureMessage(string $message, \PAB\Severity|null $level = null, \PAB\EventHint|null $hint = null)
 * @method static \PAB\EventId|null captureException(\Throwable $exception, \PAB\EventHint|null $hint = null)
 * @method static \PAB\EventId|null captureEvent(\Throwable $exception, \PAB\EventHint|null $hint = null)
 * @method static \PAB\EventId|null captureLastError(\PAB\EventHint|null $hint = null)
 * @method static bool addBreadcrumb(\PAB\Breadcrumb $breadcrumb)
 * @method static \PAB\Integration\IntegrationInterface|null getIntegration(string $className)
 * @method static \PAB\Tracing\Transaction startTransaction(\PAB\Tracing\TransactionContext $context, array $customSamplingContext = [])
 * @method static \PAB\Tracing\Transaction|null getTransaction()
 * @method static \PAB\Tracing\Span|null getSpan()
 * @method static \PAB\State\HubInterface setSpan(\PAB\Tracing\Span|null $span)
 */
class Facade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return HubInterface::class;
    }
}
