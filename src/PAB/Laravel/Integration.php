<?php

namespace PAB\Laravel;

use Illuminate\Routing\Route;
use Illuminate\Support\Str;
use PAB\PABSdk;
use PAB\Tracing\Span;
use function PAB\addBreadcrumb;
use function PAB\configureScope;
use PAB\Breadcrumb;
use PAB\Event;
use PAB\Integration\IntegrationInterface;
use PAB\State\Scope;

class Integration implements IntegrationInterface
{
    private static ?string $transaction = null;

    private static ?string $baseControllerNamespace = null;

    public function setupOnce(): void
    {
        Scope::addGlobalEventProcessor(function (Event $event): Event {
            $self = PABSdk::getCurrentHub()->getIntegration(self::class);

            if (!$self instanceof self) {
                return $event;
            }

            if (empty($event->getTransaction())) {
                $event->setTransaction($self->getTransaction());
            }

            return $event;
        });
    }

    public static function addBreadcrumb(Breadcrumb $breadcrumb): void
    {
        $self = PABSdk::getCurrentHub()->getIntegration(self::class);

        if (!$self instanceof self) {
            return;
        }

        addBreadcrumb($breadcrumb);
    }

    public static function configureScope(callable $callback): void
    {
        $self = PABSdk::getCurrentHub()->getIntegration(self::class);

        if (!$self instanceof self) {
            return;
        }

        configureScope($callback);
    }

    public static function getTransaction(): ?string
    {
        return self::$transaction;
    }

    public static function setTransaction(?string $transaction): void
    {
        self::$transaction = $transaction;
    }

    public static function setControllersBaseNamespace(?string $namespace): void
    {
        self::$baseControllerNamespace = $namespace !== null ? trim($namespace, '\\') : null;
    }

    public static function flushEvents(): void
    {
        $client = PABSdk::getCurrentHub()->getClient();

        if ($client !== null) {
            $client->flush();
        }
    }

    public static function extractNameForRoute(Route $route): ?string
    {
        $routeName = null;

        if (empty($routeName) && $route->getName()) {
            // someaction (route name/alias)
            $routeName = $route->getName();

            // Laravel 7 route caching generates a route names if the user didn't specify one
            // theirselfs to optimize route matching. These route names are useless to the
            // developer so if we encounter a generated route name we discard the value
            if (Str::contains($routeName, 'generated::')) {
                $routeName = null;
            }

            // If the route name ends with a `.` we assume an incomplete group name prefix
            // we discard this value since it will most likely not mean anything to the
            // developer and will be duplicated by other unnamed routes in the group
            if (Str::endsWith($routeName, '.')) {
                $routeName = null;
            }
        }

        if (empty($routeName) && $route->getActionName()) {
            // Some\Controller@someAction (controller action)
            $routeName = ltrim($route->getActionName(), '\\');

            $baseNamespace = self::$baseControllerNamespace ?? '';

            // Strip away the base namespace from the action name
            if (!empty($baseNamespace)) {
                // @see: Str::after, but this is not available before Laravel 5.4 so we use a inlined version
                $routeName = array_reverse(explode($baseNamespace . '\\', $routeName, 2))[0];
            }
        }

        if (empty($routeName) || $routeName === 'Closure') {
            // /someaction // Fallback to the url
            $routeName = '/' . ltrim($route->uri(), '/');
        }

        return $routeName;
    }

    public static function pabTracingMeta(): string
    {
        $span = self::currentTracingSpan();

        if ($span === null) {
            return '';
        }

        $content = sprintf('<meta name="pab-trace" content="%s"/>', $span->toTraceparent());
        // $content .= sprintf('<meta name="pab-trace-data" content="%s"/>', $span->getDescription());

        return $content;
    }

    public static function currentTracingSpan(): ?Span
    {
        return PABSdk::getCurrentHub()->getSpan();
    }
}
